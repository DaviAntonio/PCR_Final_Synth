----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Davi
-- 
-- Create Date: 23.06.2018 17:01:41
-- Design Name: 
-- Module Name: osc_a_0 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;


-- executar por 20 ms
entity osc_0 is
	generic(
		DATA_BIT_SIZE : integer := 8;
		TABLE_SIZE: integer := 28
	);
	Port (
		clk: in std_logic;
		reset: in std_logic;
		osc_0_out: out std_logic_vector((DATA_BIT_SIZE - 1) downto 0)
	);
end osc_0;
architecture Behavioral of osc_0 is
	-- Dados VHDL gerados no GNU Octave
	-- Oscilador 1
	-- Frequência 1760 Hz
	-- Taxa de amostragem 48000 Hz
	signal i : integer range 0 to TABLE_SIZE := 0;
	type t_sin_table is array(0 to (TABLE_SIZE - 1)) of integer range 0 to 255;
	constant SIN_TABLE  : t_sin_table := (127, 156, 184, 208, 229, 244, 252, 255, 250, 239, 222, 200, 174, 145, 116, 87, 60, 37, 19, 6, 0, 0, 7, 20, 39, 63, 90, 119);
begin
	gen_wave: process(clk)
	begin
		if(rising_edge(clk)) then
			if(reset = '1') then
				i <= 0;
			else
				osc_0_out <= std_logic_vector(to_unsigned(SIN_TABLE(i), osc_0_out'length));
				i <= i + 1;
				if(i = (TABLE_SIZE - 1)) then
					i <= 0;
				end if;
			end if;
		end if;
	end process;
end Behavioral;
