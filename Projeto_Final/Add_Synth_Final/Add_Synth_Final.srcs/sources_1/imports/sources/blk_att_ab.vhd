----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 23.06.2018 23:21:32
-- Design Name: 
-- Module Name: blk_att_ab - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity blk_att_ab is
	generic(
		DATA_BIT_SIZE : integer := 8
	);
	port(
		clk_fs : in std_logic;
		osc_a : in std_logic_vector((DATA_BIT_SIZE - 1) downto 0);
		osc_b : in std_logic_vector((DATA_BIT_SIZE - 1) downto 0);
		sel_att_a : in std_logic;
		sel_att_b : in std_logic;
		osc_a_out : out std_logic_vector((DATA_BIT_SIZE - 1) downto 0);
		osc_b_out : out std_logic_vector((DATA_BIT_SIZE - 1) downto 0)
	);
end blk_att_ab;

architecture Behavioral of blk_att_ab is

signal aux_att : std_logic_vector(1 downto 0);
signal aux_osc_a_out : std_logic_vector((DATA_BIT_SIZE - 1) downto 0);
signal aux_osc_b_out : std_logic_vector((DATA_BIT_SIZE - 1) downto 0);

begin

aux_att(0) <= sel_att_a;
aux_att(1) <= sel_att_b;

att_choice: process(clk_fs)
	begin
		if (rising_edge(clk_fs)) then
			case aux_att is
				when "00" =>
				aux_osc_a_out <= osc_a;
				aux_osc_b_out <= osc_b;
				
				when "01" =>
				aux_osc_a_out <= std_logic_vector(shift_left(unsigned(osc_a), 4));
				aux_osc_b_out <= osc_b;
				
				when "10" =>
				aux_osc_a_out <= osc_a;
				aux_osc_b_out <= std_logic_vector(shift_left(unsigned(osc_b), 4));
				
				when "11" =>
				aux_osc_a_out <= std_logic_vector(shift_left(unsigned(osc_a), 4));
				aux_osc_b_out <= std_logic_vector(shift_left(unsigned(osc_b), 4));
				
				when others =>
				aux_osc_a_out <= osc_a;
				aux_osc_b_out <= osc_b;
			end case;
		end if;
	end process;
	
	osc_a_out <= aux_osc_a_out; 
	osc_b_out <= aux_osc_b_out; 

end Behavioral;
