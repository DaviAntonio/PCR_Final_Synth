----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 24.06.2018 18:46:21
-- Design Name: 
-- Module Name: div_clk_int - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity div_clk_int is
    Generic ( period : integer := 2); -- real period = period * 20 ns
    
    Port ( clk : in STD_LOGIC;
		   rst : in STD_LOGIC;
           freq_clk : out STD_LOGIC);
end div_clk_int;


architecture Behavioral of div_clk_int is

signal quant : integer := 0;
signal freq_clk_s : std_logic := '0';
signal freq_clk_alarm : std_logic := '0';
--signal quant_max : integer := 2;

begin

--quant_max <= period/20;

CONTADOR: process(clk)
begin
	if (quant = period or rst = '1') then
		freq_clk_alarm <= '1';
		quant <= 0;
	else
		if rising_edge(clk) then
			freq_clk_alarm <= '0';
			quant <= quant + 1;
		end if;
	end if;
end process;

DIVISOR: process(quant)
begin
	if falling_edge(freq_clk_alarm) then
		freq_clk_s <= not(freq_clk_s);
	end if;
end process;

freq_clk <= freq_clk_s;

end Behavioral;
