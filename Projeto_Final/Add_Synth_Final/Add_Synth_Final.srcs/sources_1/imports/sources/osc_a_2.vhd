----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Davi
-- 
-- Create Date: 23.06.2018 17:01:41
-- Design Name: 
-- Module Name: osc_a_2 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity osc_2 is
	generic(
		DATA_BIT_SIZE : integer := 8;
		TABLE_SIZE: integer := 14
	);
	Port (
		clk: in std_logic;
		reset: in std_logic;
		osc_2_out: out std_logic_vector((DATA_BIT_SIZE - 1) downto 0)
	);
end osc_2;
architecture Behavioral of osc_2 is
	-- Dados VHDL gerados no GNU Octave
	-- Oscilador 3
	-- Frequência 3520 Hz
	-- Taxa de amostragem 48000 Hz
	signal i : integer range 0 to TABLE_SIZE := 0;
	type t_sin_table is array(0 to (TABLE_SIZE - 1)) of integer range 0 to 255;
	constant SIN_TABLE  : t_sin_table := (128, 185, 231, 255, 252, 224, 175, 117, 61, 19, 0, 7, 39, 91);
begin
	gen_wave: process(clk)
	begin
		if(rising_edge(clk)) then
			if(reset = '1') then
				i <= 0;
			else
				osc_2_out <= std_logic_vector(to_unsigned(SIN_TABLE(i), osc_2_out'length));
				i <= i + 1;
				if(i = (TABLE_SIZE - 1)) then
					i <= 0;
				end if;
			end if;
		end if;
	end process;
end Behavioral;