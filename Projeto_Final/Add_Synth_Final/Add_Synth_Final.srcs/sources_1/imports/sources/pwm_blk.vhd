----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 23.06.2018 23:50:12
-- Design Name: 
-- Module Name: pwm_blk - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity pwm_blk is
generic(
	-- 32 bits. 25 Hz é 4M contagens a 100 MHz 
	COUNT_BITSIZE : integer := 8
	-- ERROR: [VRFC 10-1298] range extends beyond constraint of type integer
	-- caso use 2**32 -1 
	);
	port(
	clk : in std_logic; -- 100 MHz
	reset : in std_logic;
	sig_in : in std_logic_vector((COUNT_BITSIZE - 1) downto 0);
	pwm_out : out std_logic
--	freq_pwm_out : out std_logic
	);
end pwm_blk;

architecture Behavioral of pwm_blk is
	signal val_count : std_logic_vector((COUNT_BITSIZE - 1) downto 0) := (others => '0');
	signal reg_sig_in : std_logic_vector((COUNT_BITSIZE - 1) downto 0) := (others => '0');
--	constant max_count : std_logic_vector((COUNT_BITSIZE - 1) downto 0) := std_logic_vector(resize(unsigned'(x"64"), COUNT_BITSIZE)); -- 1 MHz, 40M ns
	constant max_count : std_logic_vector((COUNT_BITSIZE - 1) downto 0) := std_logic_vector(to_unsigned(2**COUNT_BITSIZE - 1, COUNT_BITSIZE)); -- 390,625 kHz
--	signal fpwm: std_logic := '0';
--	signal fpwm_count : std_logic_vector((COUNT_BITSIZE - 1) downto 0) := (others => '0');
begin

	reg_in: process(clk, sig_in)
	begin
		if rising_edge(clk) then
			reg_sig_in <= sig_in;
		end if;
	end process;

	count_pwm: process(clk, reset, sig_in)
	
	begin
		if rising_edge(clk) then
			if reset = '1' then
				val_count <= (others => '0');
				pwm_out <= '0';
			else
				if reg_sig_in /= std_logic_vector(to_unsigned(0, reg_sig_in'length)) then
					if val_count > reg_sig_in then
						pwm_out <= '1';
					else
						pwm_out <= '0';
					end if;
					
					val_count <= std_logic_vector(unsigned(val_count) + 1);
					
	--				if (to_integer(unsigned(reg_sig_in)) = 0) then
	--					pwm_out <= '0';
	--				end if;
				else
					pwm_out <= '0';
				end if;
			end if;
		end if;
	end process;
	
--	pwm_freq_count: process(clk, reset)
--	begin
--		if rising_edge(clk) then
--			if reset = '1' then
--				fpwm <= '0';
--			else
--				if fpwm_count(COUNT_BITSIZE - 1) = '1' then
--					fpwm_count <= (others => '0');
--					fpwm <= not fpwm;
--				else
--					fpwm_count <= std_logic_vector(unsigned(fpwm_count) + 1);
--				end if;
--			end if;
--		end if;
--	end process;
	
--	fpwm <= clk;
--	freq_pwm_out <= fpwm;

end Behavioral;
