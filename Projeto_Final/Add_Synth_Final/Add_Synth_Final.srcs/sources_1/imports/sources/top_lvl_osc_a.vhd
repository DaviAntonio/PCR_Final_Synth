----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Davi
-- 
-- Create Date: 23.06.2018 17:01:41
-- Design Name: 
-- Module Name: top_lvl_osc_a - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity top_lvl_osc_a is
	generic(
		DATA_BIT_SIZE : integer := 8
	);
	Port (
		clk_fs : in std_logic;
		reset : in std_logic;
		clk_osc : in std_logic;
		act_osc : in std_logic_vector(3 downto 0);
		osc_a_blk_out : out std_logic_vector((DATA_BIT_SIZE - 1) downto 0)
	);
end top_lvl_osc_a;

architecture Behavioral of top_lvl_osc_a is
component osc_0 is
--	generic(
--		DATA_BIT_SIZE : integer := 16;
--		TABLE_SIZE: integer := 28
--	);
	Port (
		clk: in std_logic;
		reset: in std_logic;
		osc_0_out: out std_logic_vector((DATA_BIT_SIZE - 1) downto 0)
	);
end component;

component osc_1 is
--	generic(
--		DATA_BIT_SIZE : integer := 16;
--		TABLE_SIZE: integer := 10
--	);
	Port (
		clk: in std_logic;
		reset: in std_logic;
		osc_1_out: out std_logic_vector((DATA_BIT_SIZE - 1) downto 0)
	);
end component;

component osc_2 is
--	generic(
--		DATA_BIT_SIZE : integer := 16;
--		TABLE_SIZE: integer := 14
--	);
	Port (
		clk: in std_logic;
		reset: in std_logic;
		osc_2_out: out std_logic_vector((DATA_BIT_SIZE - 1) downto 0)
	);
end component;

component osc_3 is
--	generic(
--		DATA_BIT_SIZE : integer := 16;
--		TABLE_SIZE: integer := 7
--	);
	Port (
		clk: in std_logic;
		reset: in std_logic;
		osc_3_out: out std_logic_vector((DATA_BIT_SIZE - 1) downto 0)
	);
end component;

signal osc_0_out : std_logic_vector((DATA_BIT_SIZE - 1) downto 0);
signal osc_1_out : std_logic_vector((DATA_BIT_SIZE - 1) downto 0);
signal osc_2_out : std_logic_vector((DATA_BIT_SIZE - 1) downto 0);
signal osc_3_out : std_logic_vector((DATA_BIT_SIZE - 1) downto 0);

signal s_osc_blk_a_out : std_logic_vector((DATA_BIT_SIZE - 1) downto 0);

begin
oscilador_0: osc_0 port map(
	clk => clk_osc,
	reset => reset,
	osc_0_out => osc_0_out
);

oscilador_1: osc_1 port map(
	clk => clk_osc,
	reset => reset,
	osc_1_out => osc_1_out
);

oscilador_2: osc_2 port map(
	clk => clk_osc,
	reset => reset,
	osc_2_out => osc_2_out
);

oscilador_3: osc_3 port map(
	clk => clk_osc,
	reset => reset,
	osc_3_out => osc_3_out
);

-- lógica para somar
process(clk_fs, act_osc)
begin
	if(rising_edge(clk_fs)) then
		case act_osc is
			when "0000" => 
			s_osc_blk_a_out <= (others => '0');
			
			when "0001" => 
			s_osc_blk_a_out <= osc_0_out;
			
			when "0010" =>
			s_osc_blk_a_out <= osc_1_out;
			
			-- somar dividindo por 2
			when "0011" =>
			s_osc_blk_a_out <= std_logic_vector(shift_left(unsigned(osc_0_out), 1) + shift_left(unsigned(osc_1_out), 1));
			
			when "0100" =>
			s_osc_blk_a_out <= osc_2_out;
			
			-- somar dividindo por 2
			when "0101" =>
			s_osc_blk_a_out <= std_logic_vector(shift_left(unsigned(osc_0_out), 1) + shift_left(unsigned(osc_2_out), 1));
			
			-- somar dividindo 2
			when "0110" =>
			s_osc_blk_a_out <= std_logic_vector(shift_left(unsigned(osc_2_out), 1) + shift_left(unsigned(osc_1_out), 1));
			
			-- somar dividindo 4
			when "0111" =>
			s_osc_blk_a_out <= std_logic_vector(shift_left(unsigned(osc_0_out), 2) + shift_left(unsigned(osc_1_out), 2) + shift_left(unsigned(osc_0_out), 2));
			
			when "1000" =>
			s_osc_blk_a_out <= osc_3_out;
			
			-- somar dividindo 2
			when "1001" =>
			s_osc_blk_a_out <= std_logic_vector(shift_left(unsigned(osc_0_out), 1) + shift_left(unsigned(osc_3_out), 1));
			
			-- somar dividindo 2
			when "1010" =>
			s_osc_blk_a_out <= std_logic_vector(shift_left(unsigned(osc_1_out), 1) + shift_left(unsigned(osc_3_out), 1));
			
			-- somar dividindo 4
			when "1011" =>
			s_osc_blk_a_out <= std_logic_vector(shift_left(unsigned(osc_0_out), 2) + shift_left(unsigned(osc_1_out), 2) + shift_left(unsigned(osc_3_out), 2));
			
			-- somar dividindo 2
			when "1100" =>
			s_osc_blk_a_out <= std_logic_vector(shift_left(unsigned(osc_2_out), 1) + shift_left(unsigned(osc_3_out), 1));
			
			-- somar dividindo 4
			when "1101" =>
			s_osc_blk_a_out <= std_logic_vector(shift_left(unsigned(osc_0_out), 2) + shift_left(unsigned(osc_2_out), 2) + shift_left(unsigned(osc_3_out), 2));
			
			-- somar dividindo 4
			when "1110" =>
			s_osc_blk_a_out <= std_logic_vector(shift_left(unsigned(osc_1_out), 2) + shift_left(unsigned(osc_2_out), 2) + shift_left(unsigned(osc_3_out), 2));

			-- somar dividindo 4
			when "1111" =>
			s_osc_blk_a_out <= std_logic_vector(shift_left(unsigned(osc_0_out), 2) + shift_left(unsigned(osc_1_out), 2) + shift_left(unsigned(osc_2_out), 2) + shift_left(unsigned(osc_3_out), 2));
			
			when others =>		
			s_osc_blk_a_out <= (others => '0');	
		end case;
	end if;
end process;

osc_a_blk_out <= s_osc_blk_a_out;


end Behavioral;
