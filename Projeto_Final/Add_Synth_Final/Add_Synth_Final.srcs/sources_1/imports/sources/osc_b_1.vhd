----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 23.06.2018 21:42:10
-- Design Name: 
-- Module Name: osc_b_1 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity osc_5 is
	generic(
		DATA_BIT_SIZE : integer := 8;
		TABLE_SIZE: integer := 4
	);
	Port (
		clk: in std_logic;
		reset: in std_logic;
		osc_5_out: out std_logic_vector((DATA_BIT_SIZE - 1) downto 0)
	);
end osc_5;
architecture Behavioral of osc_5 is
	-- Dados VHDL gerados no GNU Octave
	-- Oscilador 6
	-- Frequência 14080 Hz
	-- Taxa de amostragem 48000 Hz
	signal i : integer range 0 to TABLE_SIZE := 0;
	type t_sin_table is array(0 to (TABLE_SIZE - 1)) of integer range 0 to 255;
	constant SIN_TABLE  : t_sin_table := (129, 255, 62, 40);
begin
	gen_wave: process(clk)
	begin
		if(rising_edge(clk)) then
			if(reset = '1') then
				i <= 0;
			else
				osc_5_out <= std_logic_vector(to_unsigned(SIN_TABLE(i), osc_5_out'length));
				i <= i + 1;
				if(i = (TABLE_SIZE - 1)) then
					i <= 0;
				end if;
			end if;
		end if;
	end process;
end Behavioral;