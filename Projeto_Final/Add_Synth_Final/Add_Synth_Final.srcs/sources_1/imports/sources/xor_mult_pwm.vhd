----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01.07.2018 01:22:19
-- Design Name: 
-- Module Name: xor_mult_pwm - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity xor_mult_pwm is
	Port (
		clk : in std_logic; -- pwm
		reset : in std_logic;
		pwm0 : in std_logic;
		pwm1 : in std_logic;
		pwm_out : out std_logic
	);
end xor_mult_pwm;

architecture Behavioral of xor_mult_pwm is

begin
	sum_pwm: process(clk, pwm0, pwm1)
	begin
		if rising_edge(clk) then
			if reset = '1' then
				pwm_out <= '0';
			
			else
				pwm_out <= pwm0 xor pwm1;
			end if;			
		end if;
	end process;

end Behavioral;
