----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 23.06.2018 21:42:10
-- Design Name: 
-- Module Name: top_lvl_osc_b - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity top_lvl_osc_b is
	generic(
	DATA_BIT_SIZE : integer := 8
);
Port (
	clk_fs : in std_logic;
	reset : in std_logic;
	clk_osc : in std_logic;
	act_osc : in std_logic_vector(3 downto 0);
	osc_b_blk_out : out std_logic_vector((DATA_BIT_SIZE - 1) downto 0)
);
end top_lvl_osc_b;

architecture Behavioral of top_lvl_osc_b is
component osc_4 is
--	generic(
--		DATA_BIT_SIZE : integer := 16;
--		TABLE_SIZE: integer := 5
--	);
	Port (
		clk: in std_logic;
		reset : in std_logic;
		osc_4_out: out std_logic_vector((DATA_BIT_SIZE - 1) downto 0)
	);
end component;

component osc_5 is
--	generic(
--		DATA_BIT_SIZE : integer := 16;
--		TABLE_SIZE: integer := 4
--	);
	Port (
		clk: in std_logic;
		reset : in std_logic;
		osc_5_out: out std_logic_vector((DATA_BIT_SIZE - 1) downto 0)
	);
end component;

component osc_6 is
--	generic(
--		DATA_BIT_SIZE : integer := 16;
--		TABLE_SIZE: integer := 3
--	);
	Port (
		clk: in std_logic;
		reset : in std_logic;
		osc_6_out: out std_logic_vector((DATA_BIT_SIZE - 1) downto 0)
	);
end component;

component osc_7 is
--	generic(
--		DATA_BIT_SIZE : integer := 16;
--		TABLE_SIZE: integer := 3
--	);
	Port (
		clk: in std_logic;
		reset : in std_logic;
		osc_7_out: out std_logic_vector((DATA_BIT_SIZE - 1) downto 0)
	);
end component;

signal osc_4_out : std_logic_vector((DATA_BIT_SIZE - 1) downto 0);
signal osc_5_out : std_logic_vector((DATA_BIT_SIZE - 1) downto 0);
signal osc_6_out : std_logic_vector((DATA_BIT_SIZE - 1) downto 0);
signal osc_7_out : std_logic_vector((DATA_BIT_SIZE - 1) downto 0);

signal s_osc_blk_b_out : std_logic_vector((DATA_BIT_SIZE - 1) downto 0);

begin
oscilador_0: osc_4 port map(
	clk => clk_osc,
	reset => reset,
	osc_4_out => osc_4_out
);

oscilador_1: osc_5 port map(
	clk => clk_osc,
	reset => reset,
	osc_5_out => osc_5_out
);

oscilador_2: osc_6 port map(
	clk => clk_osc,
	reset => reset,
	osc_6_out => osc_6_out
);

oscilador_3: osc_7 port map(
	clk => clk_osc,
	reset => reset,
	osc_7_out => osc_7_out
);

-- lógica para somar
process(clk_fs, act_osc)
begin
	if(rising_edge(clk_fs)) then
		case act_osc is
			when "0000" => 
			s_osc_blk_b_out <= (others => '0');
			
			when "0001" => 
			s_osc_blk_b_out <= osc_4_out;
			
			when "0010" =>
			s_osc_blk_b_out <= osc_5_out;
			
			-- somar dividindo por 2
			when "0011" =>
			s_osc_blk_b_out <= std_logic_vector(shift_left(unsigned(osc_4_out), 1) + shift_left(unsigned(osc_5_out), 1));
			
			when "0100" =>
			s_osc_blk_b_out <= osc_6_out;
			
			-- somar dividindo por 2
			when "0101" =>
			s_osc_blk_b_out <= std_logic_vector(shift_left(unsigned(osc_4_out), 1) + shift_left(unsigned(osc_6_out), 1));
			
			-- somar dividindo 2
			when "0110" =>
			s_osc_blk_b_out <= std_logic_vector(shift_left(unsigned(osc_6_out), 1) + shift_left(unsigned(osc_5_out), 1));
			
			-- somar dividindo 4
			when "0111" =>
			s_osc_blk_b_out <= std_logic_vector(shift_left(unsigned(osc_4_out), 2) + shift_left(unsigned(osc_5_out), 2) + shift_left(unsigned(osc_4_out), 2));
			
			when "1000" =>
			s_osc_blk_b_out <= osc_7_out;
			
			-- somar dividindo 2
			when "1001" =>
			s_osc_blk_b_out <= std_logic_vector(shift_left(unsigned(osc_4_out), 1) + shift_left(unsigned(osc_7_out), 1));
			
			-- somar dividindo 2
			when "1010" =>
			s_osc_blk_b_out <= std_logic_vector(shift_left(unsigned(osc_5_out), 1) + shift_left(unsigned(osc_7_out), 1));
			
			-- somar dividindo 4
			when "1011" =>
			s_osc_blk_b_out <= std_logic_vector(shift_left(unsigned(osc_4_out), 2) + shift_left(unsigned(osc_5_out), 2) + shift_left(unsigned(osc_7_out), 2));
			
			-- somar dividindo 2
			when "1100" =>
			s_osc_blk_b_out <= std_logic_vector(shift_left(unsigned(osc_6_out), 1) + shift_left(unsigned(osc_7_out), 1));
			
			-- somar dividindo 4
			when "1101" =>
			s_osc_blk_b_out <= std_logic_vector(shift_left(unsigned(osc_4_out), 2) + shift_left(unsigned(osc_6_out), 2) + shift_left(unsigned(osc_7_out), 2));
			
			-- somar dividindo 4
			when "1110" =>
			s_osc_blk_b_out <= std_logic_vector(shift_left(unsigned(osc_5_out), 2) + shift_left(unsigned(osc_6_out), 2) + shift_left(unsigned(osc_7_out), 2));

			-- somar dividindo 4
			when "1111" =>
			s_osc_blk_b_out <= std_logic_vector(shift_left(unsigned(osc_4_out), 2) + shift_left(unsigned(osc_5_out), 2) + shift_left(unsigned(osc_6_out), 2) + shift_left(unsigned(osc_7_out), 2));
			
			when others =>		
			s_osc_blk_b_out <= (others => '0');	
		end case;
	end if;
end process;

osc_b_blk_out <= s_osc_blk_b_out;


end Behavioral;
