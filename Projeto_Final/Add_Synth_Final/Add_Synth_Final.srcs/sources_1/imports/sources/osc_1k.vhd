----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 23.06.2018 22:52:30
-- Design Name: 
-- Module Name: osc_1k - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity osc_8 is
	generic(
		DATA_BIT_SIZE : integer := 8;
		TABLE_SIZE: integer := 49
	);
	Port (
		clk: in std_logic;
		reset: in std_logic;
		osc_8_out: out std_logic_vector((DATA_BIT_SIZE - 1) downto 0)
	);
end osc_8;
architecture Behavioral of osc_8 is
	-- Dados VHDL gerados no GNU Octave
	-- Oscilador 9
	-- Frequência 1000 Hz
	-- Taxa de amostragem 48000 Hz
	signal i : integer range 0 to TABLE_SIZE := 0;
	type t_sin_table is array(0 to (TABLE_SIZE - 1)) of integer range 0 to 255;
	constant SIN_TABLE  : t_sin_table := (127, 144, 160, 176, 191, 205, 217, 228, 237, 245, 250, 253, 255, 253, 250, 245, 237, 228, 217, 205, 191, 176, 160, 144, 127, 110, 94, 78, 63, 49, 37, 26, 17, 9, 4, 1, 0, 1, 4, 9, 17, 26, 37, 49, 63, 78, 94, 110, 127);
begin
	gen_wave: process(clk)
	begin
		if(rising_edge(clk)) then
			if(reset = '1') then
				i <= 0;
			else
				osc_8_out <= std_logic_vector(to_unsigned(SIN_TABLE(i), osc_8_out'length));
				i <= i + 1;
				if(i = (TABLE_SIZE - 1)) then
					i <= 0;
				end if;
			end if;
		end if;
	end process;
end Behavioral;