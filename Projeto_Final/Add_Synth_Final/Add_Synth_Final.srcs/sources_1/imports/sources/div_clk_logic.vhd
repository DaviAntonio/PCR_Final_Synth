----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01.07.2018 09:10:52
-- Design Name: 
-- Module Name: div_clk_logic - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity div_clk_logic is
    Port ( sel_div : in STD_LOGIC_VECTOR (3 downto 0);
           sel_AB : in STD_LOGIC; -- B em nivel alto e A em nivel baixo
           clk : in STD_LOGIC;
           rst : in STD_LOGIC;
           choosen_div : out std_logic_vector(3 downto 0);
           clkA : out STD_LOGIC;
           clkB : out STD_LOGIC;
           clk_fs : out std_logic);
end div_clk_logic;

architecture Behavioral of div_clk_logic is

component div_clk is
    Port ( clk : in STD_LOGIC;
    	   rst : in STD_LOGIC;
           clk_48kHz : out STD_LOGIC;
           clk_24kHz : out STD_LOGIC;
           clk_12kHz : out STD_LOGIC;
           clk_6kHz : out STD_LOGIC);
end component;

signal freq_div : std_logic := '0';
signal clk_48kHz, clk_24kHz, clk_12kHz, clk_6kHz : std_logic := '0';

begin

	Divisao_clock: div_clk port map (clk => clk,
		rst => rst,
		clk_48kHz => clk_48kHz,
		clk_24kHz => clk_24kHz,
		clk_12kHz => clk_12kHz,
		clk_6kHz => clk_6kHz);
	
	MuxAouB: process (clk,sel_AB, rst)
	begin
		if(rst = '1') then
			clkA <= '0';
			clkB <= '0';
		else
			if (sel_AB = '0') then -- B em nivel alto e A em nivel baixo
				clkA <= freq_div;
				clkB <= clk_48kHz; -- PARA NãO CRIAR LATCH
			else
				clkB <= freq_div;
				clkA <= clk_48kHz; --PARA NãO CRIAR LATCH
			end if;
		end if;
	end process;
	
	MuxFreq: process(clk, rst, clk_48kHz, clk_24kHz, clk_12kHz, clk_6kHz, sel_div)
	begin
		if(rst = '1') then
			freq_div <= '0';
			choosen_div <= "0000";
		else
			case sel_div is
				when "0001" =>
					freq_div <= clk_48kHz;
					choosen_div <= "0001";
					
				when "0010" =>
					freq_div <= clk_24kHz;
					choosen_div <= "0010";
					
				when "0011" =>
					freq_div <= clk_24kHz;
					choosen_div <= "0010";
					
				when "0100" =>
					freq_div <= clk_12kHz;
					choosen_div <= "0100";
					
				when "0101" =>
					freq_div <= clk_12kHz;
					choosen_div <= "0100";
					
				when "0110" =>
					freq_div <= clk_12kHz;
					choosen_div <= "0100";
					
				when "0111" =>
					freq_div <= clk_12kHz;
					choosen_div <= "0100";
					
				when "1000" =>
					freq_div <= clk_6kHz;
					choosen_div <= "1000";
					
				when "1001" =>
					freq_div <= clk_6kHz;
					choosen_div <= "1000";
					
				when "1010" =>
					freq_div <= clk_6kHz;
					choosen_div <= "1000";
					
				when "1011" =>
					freq_div <= clk_6kHz;
					choosen_div <= "1000";
					
				when "1100" =>
					freq_div <= clk_6kHz;
					choosen_div <= "1000";
					
				when "1101" =>
					freq_div <= clk_6kHz;
					choosen_div <= "1000";
					
				when "1110" =>
					freq_div <= clk_6kHz;
					choosen_div <= "1000";
					
				when "1111" =>
					freq_div <= clk_6kHz;
					choosen_div <= "1000";
					
				when others =>
					freq_div <= clk_48kHz;
					choosen_div <= "0001";				
			end case;
		end if;
	
	end process;
	
	clk_fs <= clk_48kHz;

end Behavioral;
