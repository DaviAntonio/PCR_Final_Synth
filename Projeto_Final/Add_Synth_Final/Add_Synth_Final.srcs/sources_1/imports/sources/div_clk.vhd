----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 24.06.2018 19:29:13
-- Design Name: 
-- Module Name: div_clk - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity div_clk is
    Port ( clk : in STD_LOGIC;
    	   rst : in STD_LOGIC;
           clk_48kHz : out STD_LOGIC;
           clk_24kHz : out STD_LOGIC;
           clk_12kHz : out STD_LOGIC;
           clk_6kHz : out STD_LOGIC);
end div_clk;

architecture Behavioral of div_clk is

component div_clk_int is
    Generic ( period : integer); -- real period = period * 20 ns
    Port ( clk : in STD_LOGIC;
 		   rst : in STD_LOGIC;
           freq_clk : out STD_LOGIC);
end component;

begin

map48kHz: div_clk_int
    Generic map (1042) -- real period = period * 20 ns
	Port map ( clk, rst, clk_48kHz);
	
map24kHz: div_clk_int
    Generic map (2083) -- real period = period * 20 ns
	Port map ( clk, rst, clk_24kHz);
	
map12kHz: div_clk_int
    Generic map (4167) -- real period = period * 20 ns
	Port map ( clk, rst, clk_12kHz);
	
map6kHz: div_clk_int
    Generic map (8333) -- real period = period * 20 ns
	Port map ( clk, rst, clk_6kHz);
	

end Behavioral;
