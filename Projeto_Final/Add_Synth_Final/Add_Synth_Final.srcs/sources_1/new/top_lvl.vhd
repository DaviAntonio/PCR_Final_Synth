----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01.07.2018 16:31:16
-- Design Name: 
-- Module Name: top_lvl - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity top_lvl is
	generic(
		DATA_BIT_SIZE : integer := 8;
		COUNT_BITSIZE : integer := 8
	);
	port(
		clk : in std_logic;
		
		sel_att_a : in std_logic;
		sel_att_b : in std_logic;
		sel_ring : in std_logic; -- soma em baixo e multiplica em alto
		act_osc_a : in std_logic_vector(3 downto 0);
		act_osc_b : in std_logic_vector(3 downto 0);
		sel_div : in std_logic_vector(3 downto 0); -- B em nivel alto e A em nivel baixo
		sel_div_ab : in std_logic;
		
		test_on_off : in std_logic;
		mute_play : in std_logic;
		reset : in std_logic;
		control_leds : in std_logic;
		
		led : out std_logic_vector(15 downto 0);
		pwm_signal_out : out std_logic;
		inv_pwm_signal_out : out std_logic
	);
end top_lvl;

architecture Behavioral of top_lvl is
	component top_lvl_osc_a
--		generic(
--			DATA_BIT_SIZE : integer := 8
--		);
		Port (
			clk_fs : in std_logic;
			reset : in std_logic;
			clk_osc : in std_logic;
			act_osc : in std_logic_vector(3 downto 0);
			osc_a_blk_out : out std_logic_vector((DATA_BIT_SIZE - 1) downto 0)
		);
	end component;
	
	component top_lvl_osc_b
--			generic(
--			DATA_BIT_SIZE : integer := 8
--		);
		Port (
			clk_fs : in std_logic;
			reset : in std_logic;
			clk_osc : in std_logic;
			act_osc : in std_logic_vector(3 downto 0);
			osc_b_blk_out : out std_logic_vector((DATA_BIT_SIZE - 1) downto 0)
		);
	end component;
	
	component div_clk_logic
	    Port (
		sel_div : in STD_LOGIC_VECTOR (3 downto 0);
		sel_AB : in STD_LOGIC; -- B em nivel alto e A em nivel baixo
		clk : in STD_LOGIC;
		rst : in STD_LOGIC;
		choosen_div : out std_logic_vector(3 downto 0);
		clkA : out STD_LOGIC;
		clkB : out STD_LOGIC;
		clk_fs : out std_logic);
	end component;
	
	component blk_att_ab
--		generic(
--			DATA_BIT_SIZE : integer := 8
--		);
		port(
			clk_fs : in std_logic;
			osc_a : in std_logic_vector((DATA_BIT_SIZE - 1) downto 0);
			osc_b : in std_logic_vector((DATA_BIT_SIZE - 1) downto 0);
			sel_att_a : in std_logic;
			sel_att_b : in std_logic;
			osc_a_out : out std_logic_vector((DATA_BIT_SIZE - 1) downto 0);
			osc_b_out : out std_logic_vector((DATA_BIT_SIZE - 1) downto 0)
		);
	end component;
	
	component or_sum_pwm
		Port (
			clk : in std_logic; -- pwm
			reset : in std_logic;
			pwm0 : in std_logic;
			pwm1 : in std_logic;
			pwm_out : out std_logic
		);
	end component;
	
	component osc_8
--		generic(
--			DATA_BIT_SIZE : integer := 8;
--			TABLE_SIZE: integer := 49
--		);
		Port (
			clk: in std_logic;
			reset: in std_logic;
			osc_8_out: out std_logic_vector((DATA_BIT_SIZE - 1) downto 0)
		);
	end component;
	
	component pwm_blk is
--		generic(
--		-- 32 bits. 25 Hz é 4M contagens a 100 MHz 
--		COUNT_BITSIZE : integer := 8
--		-- ERROR: [VRFC 10-1298] range extends beyond constraint of type integer
--		-- caso use 2**32 -1 
--		);
		port(
			clk : in std_logic; -- 100 MHz
			reset : in std_logic;
			sig_in : in std_logic_vector((COUNT_BITSIZE - 1) downto 0);
			pwm_out : out std_logic
--			freq_pwm_out : out std_logic
		);
	end component;
	
	component xor_mult_pwm is
		Port (
			clk : in std_logic; -- pwm
			reset : in std_logic;
			pwm0 : in std_logic;
			pwm1 : in std_logic;
			pwm_out : out std_logic
		);
	end component;
	
	signal osc_a_out :  std_logic_vector((DATA_BIT_SIZE - 1) downto 0);
	signal osc_a_out_att :  std_logic_vector((DATA_BIT_SIZE - 1) downto 0);
	signal osc_b_out :  std_logic_vector((DATA_BIT_SIZE - 1) downto 0);
	signal osc_b_out_att :  std_logic_vector((DATA_BIT_SIZE - 1) downto 0);
	signal osc_1k_out :  std_logic_vector((DATA_BIT_SIZE - 1) downto 0);
	
	signal clk_osc_a : std_logic;
	signal clk_osc_b : std_logic;
	signal clk_fs : std_logic;
--	signal fpwma : std_logic;
--	signal fpwmb : std_logic;
--	signal fpwm_test : std_logic;
	signal freq_pwm : std_logic;
	signal pwm_a_out : std_logic;
	signal pwm_b_out : std_logic;
	signal pwm_test_out : std_logic;
	
	signal or_pwm_out : std_logic;
	signal xor_pwm_out : std_logic;
	
	signal mux_or_xor_pwm_out : std_logic;
	signal mux_ab_test_pwm_out : std_logic;
	
	signal div_sel : std_logic_vector(3 downto 0);
	
	signal aux_test_or_ab : std_logic := '0';
	signal aux_mute_play : std_logic := '0';
	signal aux_led_ctrl : std_logic := '0';
	signal signal_out : std_logic;
	
	signal test_on_off_ff_d_0 : std_logic;
	signal test_on_off_ff_d_1 : std_logic;
	signal test_on_off_ff_d_delay : std_logic;
	signal test_on_off_rising : std_logic;
	
	signal mute_play_ff_d_0 : std_logic;
	signal mute_play_ff_d_1 : std_logic;
	signal mute_play_ff_d_delay : std_logic;
	signal mute_play_rising : std_logic;
	
	signal control_leds_ff_d_0 : std_logic;
	signal control_leds_ff_d_1 : std_logic;
	signal control_leds_ff_d_delay : std_logic;
	signal control_leds_rising : std_logic;
	
begin
	osc_bank_a: top_lvl_osc_a port map (
		clk_fs => clk_fs,
		reset => reset,
		clk_osc => clk_osc_a,
		act_osc => act_osc_a,
		osc_a_blk_out => osc_a_out
	);
	
	osc_bank_b: top_lvl_osc_b port map (
		clk_fs => clk_fs,
		reset => reset,
		clk_osc => clk_osc_b,
		act_osc => act_osc_b,
		osc_b_blk_out => osc_b_out
	);
	
	osc_test: osc_8
--		generic map(
--			DATA_BIT_SIZE : integer := 8;
--			TABLE_SIZE: integer := 49
--		);
		port map (
			clk => clk_fs,
			reset => reset,
			osc_8_out => osc_1k_out
		);
	
	clk_selector: div_clk_logic port map (
			sel_div => sel_div,
			sel_AB => sel_div_ab, -- B em nivel alto e A em nivel baixo
			clk => clk,
			rst => reset,
			choosen_div => div_sel,
			clkA => clk_osc_a,
			clkB => clk_osc_b,
			clk_fs => clk_fs
	);
	
	att_block: blk_att_ab
	--		generic map(
	--			DATA_BIT_SIZE : integer := 8
	--		);
		port map(
			clk_fs => clk_fs,
			osc_a => osc_a_out,
			osc_b => osc_b_out,
			sel_att_a => sel_att_a,
			sel_att_b => sel_att_b,
			osc_a_out => osc_a_out_att,
			osc_b_out => osc_b_out_att
	);
	
	pwm_a: pwm_blk
	--		generic(
	--		-- 32 bits. 25 Hz é 4M contagens a 100 MHz 
	--		COUNT_BITSIZE : integer := 8
	--		-- ERROR: [VRFC 10-1298] range extends beyond constraint of type integer
	--		-- caso use 2**32 -1 
	--		);
		port map(
			clk => clk,
			reset => reset,
			sig_in => osc_a_out_att,
			pwm_out => pwm_a_out
--			freq_pwm_out => fpwma
	);
	
	pwm_b: pwm_blk
	--		generic(
	--		-- 32 bits. 25 Hz é 4M contagens a 100 MHz 
	--		COUNT_BITSIZE : integer := 8
	--		-- ERROR: [VRFC 10-1298] range extends beyond constraint of type integer
	--		-- caso use 2**32 -1 
	--		);
		port map(
			clk => clk,
			reset => reset,
			sig_in => osc_b_out_att,
			pwm_out => pwm_b_out
--			freq_pwm_out => fpwmb
	);

	pwm_test: pwm_blk
	--		generic(
	--		-- 32 bits. 25 Hz é 4M contagens a 100 MHz 
	--		COUNT_BITSIZE : integer := 8
	--		-- ERROR: [VRFC 10-1298] range extends beyond constraint of type integer
	--		-- caso use 2**32 -1 
	--		);
		port map(
			clk => clk,
			reset => reset,
			sig_in => osc_1k_out,
			pwm_out => pwm_test_out
--			freq_pwm_out => fpwm_test
	);
	
	or_sum: or_sum_pwm
		port map (
			clk => freq_pwm,
			reset => reset,
			pwm0 => pwm_a_out,
			pwm1 => pwm_b_out,
			pwm_out => or_pwm_out
		);
		
	xor_mult: xor_mult_pwm
		port map (
			clk => freq_pwm,
			reset => reset,
			pwm0 => pwm_a_out,
			pwm1 => pwm_b_out,
			pwm_out => xor_pwm_out
		);
	
	-- lógica de decisão de somar ou multiplicar (ring mod)
	mux_ring_mod_sum: process(freq_pwm, sel_ring, or_pwm_out, xor_pwm_out)
	begin
	 -- soma em baixo e multiplica em alto
		if rising_edge(freq_pwm) then
			if sel_ring = '1' then
				mux_or_xor_pwm_out <= xor_pwm_out;
			else
				mux_or_xor_pwm_out <= or_pwm_out;
			end if;
		end if;
 	end process;
 	
 	-- lógica de decisão de ativar ou não o teste
 	-- sincronizar
 	test_on_off_ff_d_0 <= test_on_off when rising_edge(clk); -- FFD, 1a sincronização
 	test_on_off_ff_d_1 <= test_on_off_ff_d_0 when rising_edge(clk); -- FFD 2a sincronização
 	test_on_off_ff_d_delay <= test_on_off_ff_d_1 when rising_edge(clk); -- FFD 2a sincronização
 	test_on_off_rising <= not test_on_off_ff_d_delay and test_on_off_ff_d_1; -- borda de subida
 	reg_test: process(clk)
 	begin
 		-- no nested clocks
		if rising_edge(clk) then
			if test_on_off_rising = '1' then
				aux_test_or_ab <= not aux_test_or_ab;
			end if;
		end if;
 	end process;
 	mux_test_or_osc_ab: process(freq_pwm, aux_test_or_ab, pwm_test_out, mux_or_xor_pwm_out)
 	begin
 		if rising_edge(freq_pwm) then
 			if aux_test_or_ab = '0' then
 				mux_ab_test_pwm_out <= mux_or_xor_pwm_out;
 			else
 				mux_ab_test_pwm_out <= pwm_test_out;
 			end if;
 		end if;
 	end process;
 	
 	-- lógica de decisão de silenciar ou não o sistema
 	-- sincronizar
	mute_play_ff_d_0 <= mute_play when rising_edge(clk); -- FFD, 1a sincronização
	mute_play_ff_d_1 <= mute_play_ff_d_0 when rising_edge(clk); -- FFD 2a sincronização
	mute_play_ff_d_delay <= mute_play_ff_d_1 when rising_edge(clk); -- FFD 2a sincronização
	mute_play_rising <= not mute_play_ff_d_delay and mute_play_ff_d_1; -- borda de subida
 	reg_mute_play: process(clk)
 	begin
 		if rising_edge(clk) then
 			if mute_play_rising = '1' then
 				aux_mute_play <= not aux_mute_play;
 			end if;
 		end if;
 	end process;
 	mute_or_play: process(clk, mute_play, mux_ab_test_pwm_out)
 	begin
 		if rising_edge(clk) then
 			if aux_mute_play = '0' then
 				signal_out <= '0';
 			else
 				signal_out <= mux_ab_test_pwm_out;
 			end if;
 		end if;
 	end process;
 	
 	-- lógica para ativar os leds
 	-- sincronizar
	control_leds_ff_d_0 <= control_leds when rising_edge(clk); -- FFD, 1a sincronização
	control_leds_ff_d_1 <= control_leds_ff_d_0 when rising_edge(clk); -- FFD 2a sincronização
	control_leds_ff_d_delay <= control_leds_ff_d_1 when rising_edge(clk); -- FFD 2a sincronização
	control_leds_rising <= not control_leds_ff_d_delay and control_leds_ff_d_1; -- borda de subida
 	reg_leds_control: process(clk)
 	begin
 		if rising_edge(clk) then
 			if control_leds_rising = '1' then
 				aux_led_ctrl <= not aux_led_ctrl;
 			end if;
 		end if;
 	end process;
 	drive_leds: process(clk, freq_pwm, aux_led_ctrl)
 	begin
 		if rising_edge(freq_pwm) then
 			if aux_led_ctrl = '0' then -- indicar o que foi ativado
 				led(3 downto 0) <= act_osc_a;
 				led(7 downto 4) <= act_osc_b;
 				led(11 downto 8) <= div_sel;
 				led(12) <= sel_div_ab;
 				led(13) <= sel_att_a;
 				led(14) <= sel_att_b;
 				led(15) <= sel_ring;
 			else -- mostrar ondas
 				if aux_test_or_ab = '0' then
 				-- mostrar A e B
 					led(7 downto 0) <= osc_a_out_att;
 					led(15 downto 8) <= osc_b_out_att;
 				else
 				-- mostrar teste e se mudo ou não
 					led(7 downto 0) <= osc_1k_out;
 					-- se 0 é mudo
 					led(15 downto 8) <= (others => aux_mute_play);
 				end if;
 			end if;
 		end if;
 	end process;
	
	freq_pwm <= clk;
	
	pwm_signal_out <= signal_out;
	inv_pwm_signal_out <= not signal_out;

end Behavioral;
