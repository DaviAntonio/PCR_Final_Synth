----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03.07.2018 21:07:14
-- Design Name: 
-- Module Name: tb_top_lvl - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- Simular por 10 ms

entity tb_top_lvl is
--  Port ( );
	generic(
		DATA_BIT_SIZE : integer := 8;
		COUNT_BITSIZE : integer := 8
		);
end tb_top_lvl;

architecture Behavioral of tb_top_lvl is
	component top_lvl
--		generic(
--			DATA_BIT_SIZE : integer := 8;
--			COUNT_BITSIZE : integer := 8
--		);
		port(
			clk : in std_logic;
			
			sel_att_a : in std_logic;
			sel_att_b : in std_logic;
			sel_ring : in std_logic; -- soma em baixo e multiplica em alto
			act_osc_a : in std_logic_vector(3 downto 0);
			act_osc_b : in std_logic_vector(3 downto 0);
			sel_div : in std_logic_vector(3 downto 0); -- B em nivel alto e A em nivel baixo
			sel_div_ab : in std_logic;
			
			test_on_off : in std_logic;
			mute_play : in std_logic;
			reset : in std_logic;
			control_leds : in std_logic;
			
			led : out std_logic_vector(15 downto 0);
			pwm_signal_out : out std_logic;
			inv_pwm_signal_out : out std_logic
		);
	end component;
	
	signal clk : std_logic;
	
	signal sel_att_a :  std_logic;
	signal sel_att_b :  std_logic;
	signal sel_ring :  std_logic; -- soma em baixo e multiplica em alto
	signal act_osc_a :  std_logic_vector(3 downto 0);
	signal act_osc_b :  std_logic_vector(3 downto 0);
	signal sel_div :  std_logic_vector(3 downto 0); -- B em nivel alto e A em nivel baixo
	signal sel_div_ab :  std_logic;
	
	signal test_on_off :  std_logic;
	signal mute_play :  std_logic;
	signal reset :  std_logic;
	signal control_leds :  std_logic;
	
	signal led :  std_logic_vector(15 downto 0);
	signal pwm_signal_out :  std_logic;
	signal inv_pwm_signal_out :  std_logic;
	
	constant clk_period : time := 10 ns;
begin
	uut: top_lvl
--		generic(
--			DATA_BIT_SIZE : integer := 8;
--			COUNT_BITSIZE : integer := 8
--		);
		port map(
			clk => clk,
			
			sel_att_a => sel_att_a,
			sel_att_b => sel_att_b,
			sel_ring => sel_ring, -- soma em baixo e multiplica em alto
			act_osc_a => act_osc_a,
			act_osc_b => act_osc_b,
			sel_div => sel_div, -- B em nivel alto e A em nivel baixo
			sel_div_ab => sel_div_ab,
			
			test_on_off => test_on_off,
			mute_play => mute_play,
			reset => reset,
			control_leds => control_leds,
			
			led => led,
			pwm_signal_out => pwm_signal_out,
			inv_pwm_signal_out => inv_pwm_signal_out
		);
		
		resetstim: process
		begin
			reset <= '0';
			reset <= '1';
			wait for 20 ns;
			reset <= '0';
			wait;
		end process;
		
		clk_gen: process
		begin
			clk <= '0';
			wait for clk_period / 2;
			clk <= '1';
			wait for clk_period / 2;
		end process;
		
		stim: process
		begin
			wait for 20 ns;
			sel_att_a <= '0';
			sel_att_b <= '0';
			sel_ring <= '0';
			act_osc_a <= (others => '0');
			act_osc_b <= (others => '0');
			sel_div <= (others => '0');
			sel_div_ab <= '0';
			test_on_off <= '0';
			mute_play <= '0';
			control_leds <= '0';
			wait for 20 ns;
			
			-- desligar o mudo
			mute_play <= '1';
			wait for 10 us;
			mute_play <= '0';
			
			-- ligar oscilador de teste
			test_on_off <= '1';
			wait for 10 us;
			test_on_off <= '0';
			wait for 2 ms;
			
			-- desligar oscilador de teste
			test_on_off <= '1';
			wait for 10 us;
			test_on_off <= '0';
			wait for 2 ms;
			
			-- ligar oscilador a
			act_osc_a <= "0001"; -- 1760 Hz
			wait for 2*600 us;
			act_osc_a <= (others => '0');
			
			-- ligar oscilador b
			act_osc_b <= "0001"; -- 10560 Hz
			wait for 2*100 us;
			act_osc_b <= (others => '0');
			
			-- ligar ambos osciladores para somar
			act_osc_a <= "0001"; -- 1760 Hz
			act_osc_b <= "0001"; -- 10560 Hz
			wait for 2*600 us;
			
			-- ligar para multiplicar
			sel_ring <= '1';
			wait for 2*200 us;
			
			wait;
		end process;

end Behavioral;
