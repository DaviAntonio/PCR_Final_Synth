% Imprimir as amostras de um período das ondas dos osciladores

clear
clc
close all

% Arquivo para VHDLs
filename = "osc_data.txt"
arq = fopen(filename, "w")

% Frequência de amostragem das ondas
fs = 48e3;

% Numero de bits
N = 16;
% Valor máximo dos osciladores
vmax = 2^N - 1

% Numero osciladores
n_osc = 9;

% Frequência de cada um dos osciladores
fosc = [1760 5280 3520 7040 10560 14080 17600 21120 1000];
%f1 = 1760;
%f2 = 5280;
%f3 = 3520;
%f4 = 7040;
%f5 = 10560;
%f6 = 14080;
%f7 = 17600;

% Oscilador 1
%t1 = 0:1/fs:1/f1;
%osc1 = vmax*sin(2*pi*f1*t1);
%print(Dados oscilador 1)
%print(osc1)

for it = 1:n_osc
  titulo = sprintf("Oscilador %d\n", it);
  disp(titulo)
  t = 0:1/fs:1/fosc(it);
  
	for it_t = 1:length(t)
		res_sin(it_t) = sin(2*pi*fosc(it)*t(it_t));
	endfor;
  % Normalizar e converter para inteiro
	res_out = floor(vmax*((res_sin - min(res_sin))/(max(res_sin) - min(res_sin))));
	disp(res_out(1:length(t)))
  
  % Plotar no tempo
	figure;
	hold on;
	title(titulo)

	for it_p = 1:length(t)
		stem(t(it_p), res_out(it_p))
	endfor;
	
	% Plotar amostras
	figure;
	hold on;
	title(titulo)

	for it_pt = 1:length(t)
		stem(it_pt, res_out(it_pt))
	endfor;
	
	printf("\n")
	
	printf("-------------------------------------------------------------\n")
	fprintf(arq, "-------------------------------------------------------------\n")
	
	printf("Dados VHDL\n")
	fprintf(arq, "-- Dados VHDL gerados no GNU Octave\n")
	
	printf("Oscilador %d\nFrequência %d Hz\n", it, fosc(it))
	fprintf(arq, "-- Oscilador %d\n-- Frequência %d Hz\n-- Taxa de amostragem %d Hz\n",
	it, fosc(it), fs)
	
	printf("type t_sin_table is array(0 to %d) of integer range 0 to %d;\n",
	length(t) - 1, vmax)
	fprintf(arq, "type t_sin_table is array(0 to %d) of integer range 0 to %d;\n",
	length(t) - 1, vmax)
	
	printf("constant SIN_TABLE  : t_sin_table := (")
	fprintf(arq, "constant SIN_TABLE  : t_sin_table := (")
	for it_v = 1:(length(t) - 1)
		printf("%d, ", res_out(it_v))
		fprintf(arq, "%d, ", res_out(it_v))
	endfor;
	printf("%d);\n", res_out(length(t)))
	fprintf(arq, "%d);\n", res_out(length(t)))
		
	printf("\n")
endfor;

fclose(arq)
