# Sintetizador Aditivo Básico Implementado em FPGA
Projeto final da disciplina de Projeto de Circuitos Reconfiguráveis ministrada pelo professor Daniel Muñoz no primeiro semestre de 2018 para o curso de Bacharelado em Engenharia Eletrônica na Universidade de Brasília, campus Gama (FGA).

Este projeto foi escrito com a colaboração de Victor Aguiar Coutinho.

Este é um repositório criado a partir da importação do diretório que já continha o projeto final, portanto os `commits` são apenas para organização e obviamente não representam o desenvolvimento do projeto.
