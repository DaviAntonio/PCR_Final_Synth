% Imprimir as amostras de um período das ondas dos osciladores

clear
clc
close all

% Arquivo para VHDLs
filename = "osc_data.txt"
arq = fopen(filename, "w")

% Frequência de amostragem das ondas
fs = 48e3;

% Numero de bits
N = 8;
% Valor máximo dos osciladores
vmax = 2^N - 1

% Numero osciladores
n_osc = 9;

% Frequência de cada um dos osciladores
fosc = [1760 5280 3520 7040 10560 14080 17600 21120 1000];

for it = 1:n_osc
  titulo = sprintf("Oscilador %d\n", it);
  disp(titulo)
  t = 0:1/fs:1/fosc(it);
  
	for it_t = 1:length(t)
		res_sin(it_t) = sin(2*pi*fosc(it)*t(it_t));
	endfor;
  % Normalizar e converter para inteiro
	res_out = floor(vmax*((res_sin - min(res_sin))/(max(res_sin) - min(res_sin))));
	disp(res_out(1:length(t)))
  
  % Plotar no tempo
%	figure;
%	hold on;
%	title(titulo)
%	for it_p = 1:length(t)
%		stem(t(it_p), res_out(it_p))
%	endfor;
	
	% Plotar amostras
	figure;
	hold on;
	titulo_fig = sprintf("Amostras oscilador %d, f = %d Hz, fs = %d Hz",
	it - 1, fosc(it), fs) 
	titulo_arq_fig = sprintf("am_osc_%d.png", it - 1)
	title(titulo_fig)

	for it_pt = 1:length(t)
		stem(it_pt, res_out(it_pt))
	endfor;
	print("-dpng", "-r72", titulo_arq_fig)
	
	printf("\n")
	
	printf("-------------------------------------------------------------\n")
	fprintf(arq, "-------------------------------------------------------------\n")
	
	printf("Dados VHDL\n")
	
	fprintf(arq, "entity osc_%d is\n", it - 1)
	fprintf(arq, "\tgeneric(\n")
	fprintf(arq, "\t\tDATA_BIT_SIZE : integer := %d;\n", N)
	fprintf(arq, "\t\tTABLE_SIZE: integer := %d\n", length(t))
	fprintf(arq, "\t);\n")
	
	fprintf(arq, "\tPort (\n")
	fprintf(arq, "\t\tclk: in std_logic;\n")
	fprintf(arq, "\t\treset: in std_logic;\n")
	fprintf(arq, "\t\tosc_%d_out: out std_logic_vector((DATA_BIT_SIZE - 1) downto 0)\n", it - 1)
	fprintf(arq, "\t);\n")
	fprintf(arq, "end osc_%d;\n", it - 1)
	
	fprintf(arq, "architecture Behavioral of osc_%d is\n", it - 1)
	fprintf(arq, "\t-- Dados VHDL gerados no GNU Octave\n")
	fprintf(arq, "\t-- Oscilador %d\n\t-- Frequência %d Hz\n\t-- Taxa de amostragem %d Hz\n",
	it, fosc(it), fs)
	fprintf(arq, "\tsignal i : integer range 0 to TABLE_SIZE := 0;\n")
	fprintf(arq, "\ttype t_sin_table is array(0 to (TABLE_SIZE - 1)) of integer range 0 to %d;\n",
	vmax)
	fprintf(arq, "\tconstant SIN_TABLE  : t_sin_table := (")
	for it_v = 1:(length(t) - 1)
		fprintf(arq, "%d, ", res_out(it_v))
	endfor;
	fprintf(arq, "%d);\n", res_out(length(t)))
	
	fprintf(arq, "begin\n")
	fprintf(arq, "\tgen_wave: process(clk)\n")
	fprintf(arq, "\tbegin\n")
	fprintf(arq, "\t\tif(rising_edge(clk)) then\n")
	fprintf(arq, "\t\t\tif(reset = '1') then\n")
	fprintf(arq, "\t\t\t\ti <= 0;\n")
	fprintf(arq, "\t\t\telse\n")
	fprintf(arq, "\t\t\t\tosc_%d_out <= std_logic_vector(to_unsigned(SIN_TABLE(i), osc_%d_out'length));\n",
	it - 1, it - 1)
	fprintf(arq, "\t\t\t\ti <= i + 1;\n")
	fprintf(arq, "\t\t\t\tif(i = (TABLE_SIZE - 1)) then\n")
	fprintf(arq, "\t\t\t\t\ti <= 0;\n")
	fprintf(arq, "\t\t\t\tend if;\n")
	fprintf(arq, "\t\t\tend if;\n")
	fprintf(arq, "\t\tend if;\n")
	fprintf(arq, "\tend process;\n")
	fprintf(arq, "end Behavioral;\n")
	
	printf("\n")
endfor;

fclose(arq)
